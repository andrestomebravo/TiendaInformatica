package com.tiendainformatica.persistencia;

import com.tiendainformatica.mappers.FabricanteMapper;
import com.tiendainformatica.mappers.ProductoMapper;

public class GestorDatos {
	private FabricanteMapper fabricanteMapper;
	private ProductoMapper productoMapper;
	
	
	public FabricanteMapper getFabricanteMapper() {
		return fabricanteMapper;
	}
	public void setFabricanteMapper(FabricanteMapper fabricanteMapper) {
		this.fabricanteMapper = fabricanteMapper;
	}
	public ProductoMapper getProductoMapper() {
		return productoMapper;
	}
	public void setProductoMapper(ProductoMapper productoMapper) {
		this.productoMapper = productoMapper;
	}
	
	

}
