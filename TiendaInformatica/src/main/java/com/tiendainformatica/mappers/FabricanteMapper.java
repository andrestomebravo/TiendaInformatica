package com.tiendainformatica.mappers;

import java.util.List;

import com.tiendainformatica.pojos.Fabricante;
import com.tiendainformatica.pojos.Productos;

public interface FabricanteMapper {
	
	public List<Fabricante> getFabricantes();
	
	public List<Fabricante> getFabricanteProducto(int id);
	
	public int addFabricante(Fabricante fabricante);

}
