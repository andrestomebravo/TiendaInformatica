package com.tiendainformatica.mappers;

import java.util.List;
import com.tiendainformatica.pojos.*;

public interface ProductoMapper {
	
	public List<Productos> getProductos();
	
	public Productos getProducto(int id_producto);
	
	public int addProducto(Productos producto);
	
	public int editProducto(int id_producto,Productos producto);
	
	public int deleteProducto(int id_producto);
	
	public int devolverIDfabricante(int id_producto);
	

}
